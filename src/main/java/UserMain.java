import lombok.extern.slf4j.Slf4j;
import modeluser.Role;
import modeluser.User;

import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.groupingBy;
import static modeluser.Role.*;

@Slf4j
public class UserMain {
  public static void main(String[] args) {
    List<User> userList = List.of(
      new User(1, "Batman", List.of(ADMIN, USER, MODERATOR, SUPPORT)),
      new User(2, "Robin", List.of(USER, SUPPORT)),
      new User(3, "Harley", List.of(USER)),
      new User(4, "WonderWoman", List.of(SUPPORT)),
      new User(5, "Joker", List.of(ADMIN, USER)),
      new User(6, "Superman", List.of(ADMIN, MODERATOR)),
      new User(7, "Юрий", List.of(USER)),
      new User(8, "Cyborg", List.of(SUPPORT)),
      new User(9, "Shazam", List.of(USER)),
      new User(10, "Deadpool", List.of(ADMIN))
    );
    log.info(userList.toString());
    log.info("------------------------");
    List<Integer> idList = userList.stream()
      .map(User::getId)
      .toList();
    log.info(idList.toString());
    log.info("------------------------");


    List<Role> roleList = userList.stream()
      .map(User::getRole)
      .flatMap(List::stream)
      .toList();
    log.info(roleList.toString());
    log.info("------------------------");

    //1. Сделал этот лист уникальным
    List<Role> uniqueRoleList = roleList.stream()
      .distinct()
      .toList();
    log.info(uniqueRoleList.toString());
    log.info("------------------------");


    //2. Из списка user делаем Map<User.Id, User>
    Map<Integer, User> userMap = new HashMap<>();

    for (User u : userList) {
      userMap.put(u.getId(), u);
    }
    log.info(userMap.toString());
    log.info("------------------------");


    Map<Role, List<User>> roleListMap;
/*    roleListMap= Map.of(
      USER, userList,
      ADMIN, userList,
      MODERATOR, userList,
      SUPPORT, userList);*/


 //   log.info(roleListMap.toString());

  }
}
