import lombok.extern.slf4j.Slf4j;
import modellibrary.Book;
import modellibrary.EmailAddress;
import modellibrary.Reader;
import modeluser.Role;
import modeluser.User;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Slf4j
public class StreamAPIUtil {

  //инициализируем список книг
  public static List<Book> getBooks() {
    return List.of(
      new Book("Rob", "name", 1996),
      new Book("Bob", "name", 1992),
      new Book("Tom", "name", 2000),
      new Book("Som", "name", 2020),
      new Book("Thor", "name", 2010));
  }

  //инициализируем список имейлов
  public static List<EmailAddress> getMail() {
    return List.of(
      new EmailAddress("mail_1"),
      new EmailAddress("mail_2"),
      new EmailAddress("mail_3"),
      new EmailAddress("mail_4"),
      new EmailAddress("mail_5"));
  }

  // создаем читателей
  public static List<Reader> initReaders() {
    return List.of(
      new Reader("Юрий", "Email_1", true, getBooks()),
      new Reader("Андрей", "Email_2", false, getBooks()));
  }

  // получаем читателей
  public static List<Reader> getReaders() {
    return initReaders();
  }


  //сортиуем по году
  public static List<Book> sortByYear(List<Book> books) {
    return books.stream()
      .sorted(Comparator.comparing(Book::getIssueYear))
      .collect(Collectors.toList());
  }



}
