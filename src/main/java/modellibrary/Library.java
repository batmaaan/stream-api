package modellibrary;

import lombok.*;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Library {

  private List<Book> books;
  private List<Reader> readers;


}

