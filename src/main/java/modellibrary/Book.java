package modellibrary;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Book {
  private String author; //Автор
  private String name;  //Название
  private Integer issueYear; //Год издания
  }
