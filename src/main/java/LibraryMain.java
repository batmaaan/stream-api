import lombok.extern.slf4j.Slf4j;
import modellibrary.Book;
import modellibrary.EmailAddress;
import modellibrary.Library;
import modellibrary.Reader;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.mapping;

@Slf4j
public class LibraryMain {

  public static void main(String[] args) {

    List<Book> bookList = StreamAPIUtil.getBooks();
    log.info(bookList.toString());
    log.info("-------------------");

    bookList = StreamAPIUtil.sortByYear(bookList);
    log.info(bookList.toString());

    List<Reader> readers = StreamAPIUtil.initReaders();
    log.info("-------------------");
    log.info("readers has init".toUpperCase());
    log.info("-------------------");
    log.info(readers.toString());
    log.info("-------------------");

    List<Library> libraryList = List.of(new Library(bookList, readers));
    log.info("library has init".toUpperCase());
    log.info("-------------------");
    log.info(libraryList.toString());
    log.info("-------------------");


    List<EmailAddress> addresses = StreamAPIUtil.getReaders().stream()
      .map(Reader::getEmail)
      .map(EmailAddress::new).toList();
    log.info("-------------------");
    log.info("email addresses has init".toUpperCase());
    log.info("-------------------");
    log.info(addresses.toString());
    log.info("-------------------");

    List<EmailAddress> addressesWithSubscribe = StreamAPIUtil.getReaders().stream()
      .filter(Reader::isSubscriber)
      .filter(reader -> reader.getBooks().size() > 1)
      .map(Reader::getEmail).map(EmailAddress::new).toList();
    log.info("-------------------");
    log.info("only subscribers".toUpperCase());
    log.info("-------------------");
    log.info(addressesWithSubscribe.toString());
    log.info("-------------------");

    log.info("все книги взятые читателями без дубляжа");
    List<Book> booksList = StreamAPIUtil.getReaders().stream()
      .flatMap(reader -> StreamAPIUtil.getBooks().stream())
      .distinct().toList();
    log.info(booksList.toString());
    log.info("-------------------");

    log.info("кто нибудь читает тора?".toUpperCase());
    boolean match = StreamAPIUtil.getReaders().stream()
      .flatMap(reader -> reader.getBooks().stream())
      .anyMatch(book -> "Thor".equals(book.getAuthor()));
    if (match) {
      log.info("true".toUpperCase());
    } else {
      log.info("false".toUpperCase());
    }
    log.info("-------------------");

    log.info("максимально взято книг".toUpperCase());
    Integer reduce = StreamAPIUtil.getReaders().stream()
      .map(reader -> reader.getBooks().size())
      .reduce(0, (max, size) -> size > max ? size : max);
    log.info(reduce.toString());
    log.info("-------------------");

    log.info("рассылка должникам".toUpperCase());
    Map<String, List<EmailAddress>> map = StreamAPIUtil.getReaders().stream()
      .filter(Reader::isSubscriber)
      .collect(groupingBy(r -> StreamAPIUtil.getBooks().size() > 2 ? "TOO_MUCH" : "OK",
        mapping(r -> new EmailAddress(StreamAPIUtil.getMail()), Collectors.toList())));
    log.info(map.toString());
    log.info("-------------------");
  }
}
