package modeluser;

import lombok.Data;
import lombok.NoArgsConstructor;



public enum Role {
  ADMIN, USER, MODERATOR, SUPPORT

}
